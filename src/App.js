import Header from "./components/Layout/Header/Header";
import {  useState } from "react";
import Meals from "./components/Meals/Meals";
import Cart from "./components/Cart/Cart";
import CartProvider from "./store/cartProvider";

function App() {
  const [cartIsShowen, setCartIsShowen] = useState(false);
  const showCartHandler = () => {
    setCartIsShowen(true);
  };
  const hideCartHandler = () => {
    setCartIsShowen(false);
  };

  return (
    <CartProvider>
      {cartIsShowen && <Cart onHideCart={hideCartHandler} />}
      <Header
        onShowCart={showCartHandler}
       
      ></Header>
      <main>
        <Meals />
      </main>
    </CartProvider>
  );
}

export default App;
