import classes from "./AvailableMeals.module.css";
import Card from "../../UI/Card";
import MealItem from "../Mealtem/MealItem";
import { useEffect, useState } from "react";

const AvailableMeals = () => {
  const [isLoading,setIsLoading] = useState(true);
  const [hasError,setHasError] = useState();

  const[meals,setMeals]=useState([]);
  useEffect(() => {
    const fetchMeals = async () => {
      const response= await fetch(
        "https://foodorderingapp-72146-default-rtdb.firebaseio.com/meals.json"
      ).then();
        if(!response.ok){
         throw new Error ('Something went wrong!');
        }

      const responseData= await response.json();
      const loadedMeals=[];
      for(const key in responseData){
        loadedMeals.push({
          id:key,
          name:responseData[key].name,
          description:responseData[key].description,
          price:responseData[key].price
        });
      }
      setMeals(loadedMeals);
      setIsLoading(false);
    };
   
   
    fetchMeals(). catch(err=>{
    setIsLoading(false);
    setHasError(err.message);
   })

  }, []);

  if(isLoading){
    return <section className={classes.MealsLoading}>
      <p>Loading...</p>
    </section>
  }

  if(hasError)
  {
    return <section className={classes.MealsError}>
    <p>{hasError}</p>
  </section>
  } 
  

  const mealsList = meals.map((meal) => (
    <MealItem
      key={meal.id}
      id={meal.id}
      name={meal.name}
      description={meal.description}
      price={meal.price}
      {...meal}
    />
  ));
  return (
    <section className={classes.meals}>
      <Card>
        <ul>{mealsList}</ul>
      </Card>
    </section>
  );
};
export default AvailableMeals;
