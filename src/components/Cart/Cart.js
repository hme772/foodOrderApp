import classes from "./Cart.module.css";
import Modal from "../UI/Modal";
import cartContext from "../../store/cart-context";
import React, { useState ,useContext } from "react";
import CartItem from "./CartItem/CartItem";
import Checkout from "./Checkout";

const Cart = (props) => {
  const ctx = useContext(cartContext);
  const totalAmount = `$${ctx.totalAmount.toFixed(2)}`;
  const hasItems = ctx.items.length > 0 ? true : false;
  const [isCheckout, setIsCheckout] = useState(false);
  const [isSubmiting,setIsSubmiting]=useState(false);
  const [didSubmit,setDidSubmit]=useState(false);

  const cartItemRemoveHandler = (id) => {
    ctx.removeItem(id);
  };
  const cartItemAddHandler = (item) => {
    ctx.addItem({...item,amount:1});
  };
  const cartItems = (
    <ul className={classes["cart-items"]}>
      {ctx.items.map((item) => {
        return (
          <CartItem
            key={item.id}
            {...item}
            onRemove={cartItemRemoveHandler.bind(null,item.id)}
            onAdd={cartItemAddHandler.bind(null,item)}
          >
            {item.name}
          </CartItem>
        );
      })}
    </ul>
  );
  const orderHandler = () =>{
    setIsCheckout(true);
  };
  const modalActions =  <div className={classes.actions}>
  <button className={classes["button--alt"]} onClick={props.onHideCart}>
    Close
  </button>
  {hasItems && <button className={classes.button} onClick={orderHandler}>Order</button>}
</div>;

const submitOrder = async(userData) =>{
  setIsSubmiting(true);
  await fetch("https://foodorderingapp-72146-default-rtdb.firebaseio.com/orders.json",
  {method:'POST',
  body:JSON.stringify({
    user:userData,
    orderedItems:ctx.items
  })
});
  setIsSubmiting(false);
  setDidSubmit(true);
  ctx.clearCart();
};

const cardModalContent = <React.Fragment>{cartItems}
<div className={classes.total}>
  <span>Total Amount</span>
  <span>{totalAmount}</span>
</div>
{isCheckout && <Checkout onConfirm={submitOrder} onCancel={props.onClose}/>}
{!isCheckout && modalActions}
</React.Fragment>;

const isSubmitingModalContent= <p>Sending order data</p>;
const didSubmitModalContent= <React.Fragment>
  
<p>Successfully sent the order!</p>
<div className={classes.actions}>
<button className={classes.button} onClick={props.onHideCart}>
    Close
  </button>
  </div>
</React.Fragment>;

return (
    <Modal onHide={props.onHideCart}>
      {!isSubmiting && !didSubmit && cardModalContent}
      {isSubmiting && isSubmitingModalContent}
      {!isSubmiting && didSubmit && didSubmitModalContent}
    </Modal>
  );
};
export default Cart;
