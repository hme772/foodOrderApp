import classes from "./Checkout.module.css";
import { useState, useRef } from "react";

const isEmpty = (value) => value.trim().length === 0;
const isNotFiveChars = (value) => value.trim().length !== 5;
const Checkout = (props) => {
  const [formInputIsValid,setformInputIsValid]=useState({
    name:true,
    street:true,
    postal:true,
    city:true
  });
  const nameRef = useRef();
  const streetRef = useRef();
  const postalRef = useRef();
  const cityRef = useRef();

  const confirmHandler = (event) => {
    event.preventDefault();
    const enteredName = nameRef.current.value;
    const enteredStreet = streetRef.current.value;
    const enteredPostal = postalRef.current.value;
    const enteredCity = cityRef.current.value;

    const nameIsValid = !isEmpty(enteredName);
    const streetIsValid = !isEmpty(enteredStreet);
    const postalIsValid = !isNotFiveChars(enteredPostal);
    const cityIsValid = !isEmpty(enteredCity);
    
    setformInputIsValid({name:nameIsValid,
    street:streetIsValid,
    postal:postalIsValid,
    city:cityIsValid});

    const formIsValid =
      nameIsValid && 
      streetIsValid &&
      postalIsValid && 
      cityIsValid;
    if(!formIsValid) return;

    props.onConfirm({
        name:enteredName,
        street:enteredStreet,
        postal:enteredPostal,
        city:enteredCity
    });
  };

  return (
    <form className={classes.form} onSubmit={confirmHandler}>
      <div className={`${classes.control} ${formInputIsValid.name ? '' : classes.invalid}`}>
        <label htmlFor="name">Your Name</label>
        <input ref={nameRef} type="text" id="name" />
      {!formInputIsValid.name && <p>please enter a valid name</p>}
      </div>
      <div className={`${classes.control} ${formInputIsValid.street ? '' : classes.invalid}`}>
        <label htmlFor="street">Street</label>
        <input ref={streetRef} type="text" id="street" />
        {!formInputIsValid.street && <p>please enter a valid street</p>}
      </div>
      <div className={`${classes.control} ${formInputIsValid.postal ? '' : classes.invalid}`}>
        <label htmlFor="postal">Postal Code</label>
        <input ref={postalRef} type="text" id="postal" />
        {!formInputIsValid.postal && <p>please enter a 5 chars long postal code</p>}
      </div>
      <div className={`${classes.control} ${formInputIsValid.city ? '' : classes.invalid}`}>
        <label htmlFor="city">City</label>
        <input ref={cityRef} type="text" id="city" />
        {!formInputIsValid.city && <p>please enter a valid city</p>}
      </div>
      <div className={classes.actions}>
        <button type="button" onClick={props.onCancel}>
          Cancel
        </button>
        <button className={classes.submit}>Confirm</button>
      </div>
    </form>
  );
};

export default Checkout;
